import 'package:flutter_test/flutter_test.dart';

import 'package:king011_flutter/king011_flutter.dart';

const _StrError = "not support empty";
const _IntError = "must >= 10";
const _BoolError = "only support true";
void main() {
  test('FormValidatorState autovalidate', () {
    /// new formKey and validator
    var form = Map<String, dynamic>();
    var validator = FormValidatorState(fields: {
      "str": FieldValidatorState<String>(
        initialValue: "init",
        validator: (validator, v) => v.isEmpty ? _StrError : null,
        onSaved: (v) => form["str"] = v,
        autovalidate: true,
      ),
      "int": FieldValidatorState<int>(
        initialValue: 5,
        validator: (validator, v) => v < 10 ? _IntError : null,
        onSaved: (v) => form["int"] = v,
        autovalidate: true,
      ),
      "bool": FieldValidatorState<bool>(
        initialValue: false,
        validator: (validator, v) => !v ? _BoolError : null,
        onSaved: (v) => form["bool"] = v,
        autovalidate: true,
      ),
    });

    /// set validator to form filed

    expect(validator.initialValue("str"), "init");
    expect(validator.autovalidate("str"), true);
    expect(validator.validator("str") != null, true);
    expect(validator.onSaved("str") != null, true);
    expect(validator.initialValue("int"), 5);
    expect(validator.autovalidate("int"), true);
    expect(validator.validator("int") != null, true);
    expect(validator.onSaved("int") != null, true);
    expect(validator.initialValue("bool"), false);
    expect(validator.autovalidate("bool"), true);
    expect(validator.validator("bool") != null, true);
    expect(validator.onSaved("bool") != null, true);

    // if not key return false/null
    expect(validator.initialValue("no"), null);
    expect(validator.autovalidate("no"), false);
    expect(validator.validator("no"), null);
    expect(validator.onSaved("no"), null);

    /// simulator

    // val == initVal not set dirty
    expect(validator.fields["str"].dirty, false);
    expect(validator.fields["str"].validator("init"), null);
    expect(validator.fields["str"].dirty, false);
    // val != initVal so set diry and call validator func
    expect(validator.fields["str"].validator(""), "not support empty");
    expect(validator.fields["str"].dirty, true);
    expect(validator.fields["str"].validator("123"), null);

    // val == initVal not set dirty
    // dirty == false so not call validator func
    expect(validator.fields["int"].dirty, false);
    expect(validator.fields["int"].validator(5), null);
    expect(validator.fields["int"].dirty, false);
    // val != initVal so set diry and call validator func
    expect(validator.fields["int"].validator(9), _IntError);
    expect(validator.fields["int"].dirty, true);
    expect(validator.fields["int"].validator(10), null);

    // val == initVal not set dirty
    // dirty == false so not call validator func
    expect(validator.fields["bool"].dirty, false);
    expect(validator.fields["bool"].validator(false), null);
    expect(validator.fields["bool"].dirty, false);
    // val != initVal so set diry and call validator func
    expect(validator.fields["bool"].validator(true), null);
    expect(validator.fields["bool"].dirty, true);
    expect(validator.fields["bool"].validator(false), _BoolError);
    expect(validator.fields["bool"].dirty, true);
    expect(validator.fields["bool"].validator(true), null);

    /// reset
    validator.reset();
    expect(validator.fields["str"].dirty, false);
    expect(validator.fields["int"].dirty, false);
    expect(validator.fields["bool"].dirty, false);
  });
}
