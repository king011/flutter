import 'package:flutter/material.dart';
import 'package:king011_flutter/king011_flutter.dart';

class MyTreeView extends StatefulWidget {
  MyTreeView({Key key}) : super(key: key);

  @override
  _MyTreeViewState createState() => _MyTreeViewState();
}

class _MyTreeViewState extends State<MyTreeView> {
  // create tree controller
  final TreeController _controller = TreeController(
    // Optional sort
    sort: (l, r) {
      return l.text.compareTo(r.text);
    },
  );

  @override
  void initState() {
    super.initState();
    // creat init tree item
    _reset();
  }

  _clear() {
    // clear items
    _controller.clear();
  }

  _getChecked() {
    debugPrint("forEach");
    _controller.forEach((controller) {
      debugPrint("${controller.text} ${controller.checked}");
    });

    debugPrint("forChecked");
    _controller.forChecked((controller) {
      debugPrint("${controller.text} ${controller.checked}");
    });
    debugPrint("forTopChecked");
    _controller.forTopChecked((controller) {
      debugPrint("${controller.text} ${controller.checked}");
    });
  }

  _reset() {
    // init items
    _controller
      ..clear()
      ..push(
        null,
        TreeValue(
          id: "cd",
          text: "成都",
          expand: true,
        ),
      )
      ..push(
        "cd",
        TreeValue(
          id: "cd_ch",
          text: "成華區",
        ),
      )
      ..push(
        "cd_ch",
        TreeValue(
          id: "cd_ch_ltlj",
          text: "龍潭立交",
          checked: true,
        ),
      )
      ..push(
        "cd",
        TreeValue(
          id: "cd_jj",
          text: "錦江區",
          checked: true,
        ),
      )
      ..push(
        "cd",
        TreeValue(
          id: "cd_wj",
          text: "溫江區",
        ),
      )
      ..push(
        null,
        TreeValue(
          id: "hz",
          text: "杭州",
        ),
      );
  }

  var _parentController = TextEditingController();
  var _idController = TextEditingController();
  var _textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("tree view"),
      ),
      body: ListView(
        // set validators to fields
        children: <Widget>[
          Card(
            child: SingleChildScrollView(
              padding: EdgeInsets.all(8),
              child: TreeView(
                controller: _controller,
                emptyBuilder: (context) {
                  return Text("tree empty");
                },
                builder: (context, item) {
                  debugPrint("build ${item.text}");
                  return Row(
                    children: <Widget>[
                      ConstrainedBox(
                        constraints: BoxConstraints(
                          maxHeight: 24,
                        ),
                        child: Checkbox(
                          value: item.checked,
                          onChanged: (v) {
                            debugPrint("onChanged ${item.text} $v");
                            item.checked = v;
                          },
                        ),
                      ),
                      Expanded(
                        child: Text("${item.text} ${item.id}"),
                      ),
                      Offstage(
                        offstage: !item.children,
                        child: Container(
                          height: 26,
                          child: IconButton(
                            padding: EdgeInsets.all(0),
                            icon: item.expand
                                ? Icon(Icons.expand_less)
                                : Icon(Icons.expand_more),
                            onPressed: () {
                              item.expand = !item.expand;
                            },
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
          Card(
            child: Container(
              padding: EdgeInsets.only(
                top: 8,
                left: 8,
                right: 8,
              ),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "input parent id",
                    ),
                    controller: _parentController,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "input id",
                    ),
                    controller: _idController,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "input text",
                    ),
                    controller: _textController,
                  ),
                  ButtonBar(
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          final parentID = _parentController.text;
                          final id = _idController.text;
                          final text = _textController.text;
                          bool ok = _controller.push(
                            parentID,
                            TreeValue(
                              id: id,
                              text: text,
                            ),
                          );
                          debugPrint("add $ok");
                        },
                        child: Text("add"),
                      ),
                      RaisedButton(
                        onPressed: () {
                          final id = _idController.text;
                          final controller = _controller.controller(id);
                          if (controller != null) {
                            final text = _textController.text;
                            controller.text = text;
                          }
                        },
                        child: Text("modify"),
                      ),
                      RaisedButton(
                        onPressed: () {
                          final id = _idController.text;
                          final ok = _controller.remove(id);
                          debugPrint("remove $ok");
                        },
                        child: Text("remove"),
                      ),
                    ],
                  ),
                  ButtonBar(
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          final id = _idController.text;
                          final controller = _controller.controller(id);
                          if (controller != null) {
                            controller.checked = !controller.checked;
                          }
                        },
                        child: Text("!checked"),
                      ),
                      RaisedButton(
                        onPressed: () {
                          final id = _idController.text;
                          final controller = _controller.controller(id);
                          if (controller != null) {
                            controller.expand = !controller.expand;
                          }
                        },
                        child: Text("!expand"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          ButtonBar(
            children: <Widget>[
              RaisedButton(
                onPressed: _getChecked,
                child: Text("get checked"),
              ),
              RaisedButton(
                onPressed: _clear,
                child: Text("Clear"),
              ),
              RaisedButton(
                onPressed: _reset,
                child: Text("Reset"),
              ),
            ],
          )
        ],
      ),
    );
  }
}
