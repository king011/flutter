import 'package:flutter/material.dart';
import './validator/validator_auto.dart';
import './validator/validator_manual.dart';
import './widget/tree_view.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
      routes: {
        "/validator/auto": (_) => MyValidatorAutoPage(),
        "/validator/manual": (_) => MyValidatorManualPage(),
        "/widget/treeview": (_) => MyTreeView(),
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("king011_flutter Example"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(12),
          child: MyExpansionPanelList(),
        ),
      ),
    );
  }
}

/// 派生 一個 StatefulWidget 來 管理 面板展開
class MyExpansionPanelList extends StatefulWidget {
  MyExpansionPanelList({Key key}) : super(key: key);
  @override
  _MyExpansionPanelListState createState() => _MyExpansionPanelListState();
}

class _MyExpansionPanelListState extends State<MyExpansionPanelList> {
  /// 定義 當前展開的 面板
  final List<Panel> _panels = panels;
  @override
  Widget build(BuildContext context) {
    return ExpansionPanelList(
      /// 每當 面板 展開 關閉時 會回調此 函數
      expansionCallback: (panelIndex, isExpanded) {
        final expanded = !isExpanded;
        if (expanded != _panels[panelIndex].expanded) {
          setState(() {
            _panels[panelIndex].expanded = expanded;
          });
        }
      },

      /// 子面板 數組
      children: _panels.map((Panel paenl) {
        return ExpansionPanel(
          isExpanded: paenl.expanded,
          headerBuilder: (context, isExpanded) => ListTile(
                dense: true,
                title: Text('${paenl.title}'),
              ),
          body: Container(
            height: 160,
            child: ListView(
              children: paenl.items.map((Item item) {
                return ListTile(
                  dense: true,
                  leading: Icon(Icons.event),
                  title: Text(item.title),
                  onTap: () => Navigator.of(context).pushNamed(item.path),
                );
              }).toList(),
            ),
          ),
        );
      }).toList(),
    );
  }
}

/// 定義面板 數據
class Panel {
  String title;
  bool expanded;
  List<Item> items;
  Panel({
    this.title,
    this.expanded = false,
    @required this.items,
  }) : assert(items != null);
}

class Item {
  String title;
  String path;
  Item(this.title, this.path);
}

final List<Panel> panels = <Panel>[
  Panel(
    title: 'validator',
    expanded: true,
    items: <Item>[
      Item("validator auto ", "/validator/auto"),
      Item("validator manual", "/validator/manual"),
    ],
  ),
  Panel(
    title: 'widget',
    items: <Item>[
      Item("tree view ", "/widget/treeview"),
    ],
  )
];
