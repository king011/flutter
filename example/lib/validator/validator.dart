import 'package:king011_flutter/king011_flutter.dart';

String validatorString(FieldValidatorState<String> validator, String val) {
  val = val.toString();
  if (val.length < 4) {
    return "val.length < 4";
  }
  return null;
}

String validatorDouble(FieldValidatorState<double> validator, double val) {
  if (val < 80) {
    return "val < 80";
  }
  return null;
}

String validatorBool(FieldValidatorState<bool> validator, bool val) {
  if (!val) {
    return "only support true";
  }
  return null;
}
