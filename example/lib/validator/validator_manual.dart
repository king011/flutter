import 'package:flutter/material.dart';
import 'package:king011_flutter/king011_flutter.dart';
import './validator.dart';

class MyValidatorManualPage extends StatefulWidget {
  MyValidatorManualPage({Key key}) : super(key: key);

  @override
  _MyValidatorManualPageState createState() => _MyValidatorManualPageState();
}

const _FieldStr = "str test";
const _FieldDouble = "double test";
const _FieldBool = "bool test";

class _MyValidatorManualPageState extends State<MyValidatorManualPage> {
  // define form and state
  GlobalKey<FormState> _form = GlobalKey<FormState>();
  FormValidatorState _validator;
  String _str;
  double _double;
  bool _bool;

  @override
  void initState() {
    super.initState();

    // init validator
    //
    // set all fields initialValue/onSaved/validator at here
    _validator = FormValidatorState(fields: {
      _FieldStr: FieldValidatorState<String>(
        initialValue: "kk",
        autovalidate: true,
        autoinvalid: false,
        onSaved: (v) => _str = v.trim(),
        validator: (validator, val) {
          // deal diry and invalid
          if (validator.invalid) {
            // initialValue changed set dirty
            if (validator.initialValue != val) {
              validator.dirty = true;
              return validatorString(validator, val);
            }
            return null;
          } else if (!validator.dirty) {
            validator.dirty = true;
          }
          // validator
          return validatorString(validator, val);
        },
      ),
      _FieldDouble: FieldValidatorState<double>(
        initialValue: 10,
        autovalidate: true,
        autoinvalid: false,
        onSaved: (v) => _double = v,
        validator: (validator, val) {
          // deal diry and invalid
          if (validator.invalid) {
            // initialValue changed set dirty
            if (validator.initialValue != val) {
              validator.dirty = true;
              return validatorDouble(validator, val);
            }
            return null;
          } else if (!validator.dirty) {
            validator.dirty = true;
          }
          // validator
          return validatorDouble(validator, val);
        },
      ),
      _FieldBool: FieldValidatorState<bool>(
        initialValue: false,
        autovalidate: true,
        autoinvalid: false,
        onSaved: (v) => _bool = v,
        validator: (validator, val) {
          // deal diry and invalid
          if (validator.invalid) {
            // initialValue changed set dirty
            if (validator.initialValue != val) {
              validator.dirty = true;
              return validatorBool(validator, val);
            }
            return null;
          } else if (!validator.dirty) {
            validator.dirty = true;
          }
          return validatorBool(validator, val);
        },
      ),
    });

    // reset initialValue
    _textEditingController.text = _validator.initialValue(_FieldStr);
  }

  _reset() {
    // reset form to not dirty
    _validator.reset();
    // reset form
    _form.currentState.reset();

    // reset initialValue
    _textEditingController.text = _validator.initialValue(_FieldStr);
    var offset = _textEditingController.text.length;
    if (offset != 0) {
      _textEditingController.selection = TextSelection(
        baseOffset: offset,
        extentOffset: offset,
      );
    }
  }

  _submit() {
    // validate form
    final form = _form.currentState;
    if (!_validator.validate(form)) {
      setState(() {});
      return;
    }
    // save form
    form.save();

    // net request or other submit anything
    debugPrint("_str = $_str");
    debugPrint("_double = $_double");
    debugPrint("_bool = $_bool");
  }

  // create controllers
  var _textEditingController = TextEditingController();
  var _sliderController = SliderController.fromValue(
    min: 0,
    max: 100,
    value: 0,
  );
  var _switchController = SwitchController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("validator manual"),
      ),
      body: Form(
        key: _form,
        child: Container(
          padding: EdgeInsets.all(16),
          alignment: FractionalOffset(0.5, 0.2),
          child: ListView(
            // set validators to fields
            children: <Widget>[
              TextFormField(
                autofocus: true,
                autovalidate: _validator?.autovalidate(_FieldStr),
                validator: _validator?.validator(_FieldStr),
                onSaved: _validator?.onSaved(_FieldStr),
                decoration: InputDecoration(
                  labelText: "input your name",
                ),
                controller: _textEditingController,
              ),
              SliderFormField(
                  initialValue: _validator?.initialValue(_FieldDouble),
                  autovalidate: _validator?.autovalidate(_FieldDouble),
                  validator: _validator?.validator(_FieldDouble),
                  onSaved: _validator?.onSaved(_FieldDouble),
                  leadingLabel: "love",
                  controller: _sliderController,
                  trailingLabel: "${_sliderController.getValue().floor()}",
                  divisions: 100,
                  label: "${_sliderController.getValue().floor()}",
                  onChanged: (_) {
                    setState(() {});
                  }),
              SwitchFormField(
                initialValue: _validator?.initialValue(_FieldBool),
                autovalidate: _validator?.autovalidate(_FieldBool),
                validator: _validator?.validator(_FieldBool),
                onSaved: _validator?.onSaved(_FieldBool),
                leadingLabel: "open",
                controller: _switchController,
                trailingLabel: _switchController.value ? "on" : "off",
                onChanged: (_) {
                  setState(() {});
                },
              ),
              ButtonBar(
                children: <Widget>[
                  RaisedButton(
                    onPressed: _reset,
                    child: Text("Reset"),
                  ),
                  RaisedButton(
                    onPressed: _submit,
                    child: Text("Submit"),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
