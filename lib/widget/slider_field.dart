import 'package:flutter/material.dart';

class SliderValue {
  final double _min;
  final double _max;
  double _value;
  SliderValue({
    @required double value,
    double min,
    double max,
  })  : assert(value != null),
        assert(min != null),
        assert(max != null),
        assert(min < max),
        assert(min <= value && value <= max),
        _min = min,
        _max = max,
        _value = value;

  set value(double v) {
    if (v < _min) {
      v = _min;
    } else if (v > _max) {
      v = _max;
    }

    if (v != _value) {
      _value = v;
    }
  }

  double get value => _value;

  @override
  bool operator ==(dynamic other) {
    if (identical(this, other)) return true;
    if (other is! SliderValue) return false;
    final SliderValue typedOther = other;
    return typedOther._min == _min &&
        typedOther._max == _max &&
        typedOther._value == _value;
  }

  @override
  int get hashCode => hashValues(_min.hashCode, _max.hashCode, _value.hashCode);
}

/// Slider Controller for SliderField
class SliderController extends ValueNotifier<SliderValue> {
  SliderController({
    SliderValue val,
  }) : super(val ?? SliderValue(min: 0, max: 1, value: 0));
  SliderController.fromController(SliderController controller)
      : super(SliderValue(
          min: controller.value._min,
          max: controller.value._max,
          value: controller.value._value,
        ));
  SliderController.fromValue({
    double min,
    double max,
    @required double value,
  }) : super(SliderValue(
          min: min,
          max: max,
          value: value,
        ));

  double getValue() {
    return value.value;
  }

  void setValue(double newValue) {
    var oldValue = value.value;
    value.value = newValue;
    if (oldValue != value.value) {
      // 通知 更新
      notifyListeners();
    }
  }

  double get min => value._min;
  double get max => value._max;
}

/// Slider Field Wrapper
class SliderField extends StatefulWidget {
  SliderField({
    Key key,
    this.leadingLabel,
    SliderController controller,
    this.trailingLabel,
    this.divisions,
    this.label,
    this.onChanged,
    this.onChangeStart,
    this.onChangeEnd,
    this.enabled = true,
    this.errorText,
    this.activeColor,
    this.inactiveColor,
    this.semanticFormatterCallback,
  })  : assert(enabled != null),
        controller = controller ?? SliderController(),
        super(
          key: key,
        );
  final SliderController controller;

  final bool enabled;

  /// Text to place at the start of the horizontal leading.
  final String leadingLabel;

  /// Text to place at the end of the horizontal slider.
  final String trailingLabel;

  /// Slider.divisions
  final int divisions;

  /// Slider.label
  final String label;

  /// Slider.activeColor
  final Color activeColor;

  /// Slider.inactiveColor
  final Color inactiveColor;

  /// Slider.semanticFormatterCallback
  final SemanticFormatterCallback semanticFormatterCallback;

  /// Slider.onChanged
  final ValueChanged<double> onChanged;

  /// Slider.onChanged
  final ValueChanged<double> onChangeStart;

  /// Slider.onChanged
  final ValueChanged<double> onChangeEnd;

  final String errorText;

  @override
  _SliderFieldState createState() => _SliderFieldState();
}

class _SliderFieldState extends State<SliderField> {
  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_handleControllerChanged);
  }

  @override
  void dispose() {
    widget.controller.removeListener(_handleControllerChanged);
    super.dispose();
  }

  void _handleControllerChanged() {
    setState(() {
      if (widget.onChanged != null) {
        widget.onChanged(widget.controller.getValue());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> children = <Widget>[];
    final errorColor = Theme.of(context).errorColor;
    if (widget.leadingLabel != null) {
      children.add(Text(
        widget.leadingLabel,
        style: widget.errorText == null ? null : TextStyle(color: errorColor),
      ));
    }

    children.add(Expanded(
      child: Slider(
        min: widget.controller.min,
        max: widget.controller.max,
        value: widget.controller.value.value,
        onChangeStart: widget.onChangeStart,
        onChanged: widget.enabled
            ? (v) {
                if (v != widget.controller.value.value) {
                  setState(() {
                    widget.controller.value.value = v;
                  });
                }
                if (widget.onChanged != null) {
                  widget.onChanged(widget.controller.getValue());
                }
              }
            : null,
        onChangeEnd: widget.onChangeEnd,
        label: widget.label,
        divisions: widget.divisions,
        activeColor: widget.errorText == null ? widget.activeColor : errorColor,
        inactiveColor: widget.inactiveColor,
        semanticFormatterCallback: widget.semanticFormatterCallback,
      ),
    ));

    if (widget.trailingLabel != null) {
      children.add(Text(
        widget.trailingLabel,
        style: widget.errorText == null ? null : TextStyle(color: errorColor),
      ));
    }
    if (widget.errorText == null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: children,
      );
    } else {
      return Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: children,
          ),
          ListBody(
            children: <Widget>[
              Text(
                widget.errorText,
                style: TextStyle(
                  color: Theme.of(context).errorColor,
                ),
              )
            ],
          ),
        ],
      );
    }
  }
}

/// SliderField Form Wrapper
class SliderFormField extends FormField<double> {
  SliderFormField({
    Key key,
    @required this.controller,
    double initialValue,
    String leadingLabel,
    String trailingLabel,
    int divisions,
    String label,
    ValueChanged<double> onChanged,
    ValueChanged<double> onChangeStart,
    ValueChanged<double> onChangeEnd,
    Color activeColor,
    Color inactiveColor,
    SemanticFormatterCallback semanticFormatterCallback,
    bool autovalidate = false,
    FormFieldSetter<double> onSaved,
    FormFieldValidator<double> validator,
    bool enabled = true,
  })  : assert(controller != null),
        super(
          key: key,
          initialValue: initialValue ?? controller.getValue(),
          autovalidate: autovalidate,
          onSaved: onSaved,
          validator: validator,
          enabled: enabled,
          builder: (FormFieldState<double> field) {
            return SliderField(
              key: key,
              enabled: enabled,
              controller: controller,
              leadingLabel: leadingLabel,
              trailingLabel: trailingLabel,
              divisions: divisions,
              label: label,
              activeColor: activeColor,
              inactiveColor: inactiveColor,
              semanticFormatterCallback: semanticFormatterCallback,
              onChanged: onChanged,
              onChangeStart: onChangeStart,
              onChangeEnd: onChangeEnd,
              errorText: field.errorText,
            );
          },
        );
  final SliderController controller;

  @override
  _SliderFormFieldState createState() => _SliderFormFieldState();
}

class _SliderFormFieldState extends FormFieldState<double> {
  @override
  SliderFormField get widget => super.widget;

  @override
  void initState() {
    super.initState();
    if (widget.controller.getValue() != widget.initialValue) {
      widget.controller.setValue(widget.initialValue);
    }
    widget.controller.addListener(_handleControllerChanged);
  }

  @override
  void didUpdateWidget(SliderFormField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.removeListener(_handleControllerChanged);
      widget.controller.addListener(_handleControllerChanged);
    }
    setValue(widget.controller.getValue());
  }

  @override
  void dispose() {
    widget.controller.removeListener(_handleControllerChanged);
    super.dispose();
  }

  @override
  void reset() {
    super.reset();

    widget.controller.setValue(widget.initialValue);
  }

  void _handleControllerChanged() {
    if (value != widget.controller.getValue()) {
      didChange(widget.controller.getValue());
    }
  }
}
