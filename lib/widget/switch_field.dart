import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

/// Switch Controller for SwitchField
class SwitchController extends ValueNotifier<bool> {
  SwitchController({
    bool val,
  }) : super(val ?? false);

  bool getValue() {
    return value;
  }

  void setValue(bool newValue) {
    if (value != newValue) {
      value = newValue;
      // 通知 更新
      notifyListeners();
    }
  }
}

/// Switch Field Wrapper
class SwitchField extends StatefulWidget {
  SwitchField({
    Key key,
    this.leadingLabel,
    @required this.controller,
    this.trailingLabel,
    this.enabled = true,
    this.expanded = false,
    this.errorText,
    this.onChanged,
    this.activeColor,
    this.activeTrackColor,
    this.inactiveThumbColor,
    this.inactiveTrackColor,
    this.activeThumbImage,
    this.inactiveThumbImage,
    this.materialTapTargetSize,
    this.dragStartBehavior = DragStartBehavior.down,
  })  : assert(enabled != null),
        assert(expanded != null),
        assert(controller != null),
        super(
          key: key,
        );
  final SwitchController controller;

  final bool enabled;
  final bool expanded;

  /// Text to place at the start of the horizontal leading.
  final String leadingLabel;

  /// Text to place at the end of the horizontal slider.
  final String trailingLabel;

  final String errorText;

  final ValueChanged<bool> onChanged;
  final Color activeColor;
  final Color activeTrackColor;
  final Color inactiveThumbColor;
  final Color inactiveTrackColor;
  final ImageProvider activeThumbImage;
  final ImageProvider inactiveThumbImage;
  final MaterialTapTargetSize materialTapTargetSize;
  final DragStartBehavior dragStartBehavior;

  @override
  _SwitchFieldState createState() => _SwitchFieldState();
}

class _SwitchFieldState extends State<SwitchField> {
  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_handleControllerChanged);
  }

  @override
  void dispose() {
    widget.controller.removeListener(_handleControllerChanged);
    super.dispose();
  }

  void _handleControllerChanged() {
    setState(() {
      if (widget.onChanged != null) {
        widget.onChanged(widget.controller.getValue());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> children = <Widget>[];
    final errorColor = Theme.of(context).errorColor;
    if (widget.leadingLabel != null) {
      children.add(Text(
        widget.leadingLabel,
        style: widget.errorText == null ? null : TextStyle(color: errorColor),
      ));
    }

    var instance = Switch(
      value: widget.controller.value,
      onChanged: widget.enabled
          ? (v) {
              if (v != widget.controller.value) {
                setState(() {
                  widget.controller.value = v;
                });
              }
              if (widget.onChanged != null) {
                widget.onChanged(widget.controller.getValue());
              }
            }
          : null,
      activeColor: widget.errorText == null ? widget.activeColor : errorColor,
      activeTrackColor: widget.activeTrackColor,
      inactiveThumbColor: widget.inactiveThumbColor,
      inactiveTrackColor: widget.inactiveTrackColor,
      activeThumbImage: widget.activeThumbImage,
      inactiveThumbImage: widget.inactiveThumbImage,
      materialTapTargetSize: widget.materialTapTargetSize,
      dragStartBehavior: widget.dragStartBehavior,
    );
    if (widget.expanded) {
      children.add(Expanded(child: instance));
    } else {
      children.add(instance);
    }

    if (widget.trailingLabel != null) {
      children.add(Text(
        widget.trailingLabel,
        style: widget.errorText == null ? null : TextStyle(color: errorColor),
      ));
    }
    if (widget.errorText == null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: children,
      );
    } else {
      return Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: children,
          ),
          ListBody(
            children: <Widget>[
              Text(
                widget.errorText,
                style: TextStyle(
                  color: Theme.of(context).errorColor,
                ),
              )
            ],
          ),
        ],
      );
    }
  }
}

/// SwitchField Form Wrapper
class SwitchFormField extends FormField<bool> {
  SwitchFormField({
    Key key,
    SwitchController controller,
    bool initialValue,
    String leadingLabel,
    String trailingLabel,
    ValueChanged<bool> onChanged,
    bool autovalidate = false,
    FormFieldSetter<bool> onSaved,
    FormFieldValidator<bool> validator,
    bool enabled = true,
    Color activeColor,
    Color activeTrackColor,
    Color inactiveThumbColor,
    Color inactiveTrackColor,
    ImageProvider activeThumbImage,
    ImageProvider inactiveThumbImage,
    MaterialTapTargetSize materialTapTargetSize,
    DragStartBehavior dragStartBehavior = DragStartBehavior.down,
  })  : assert(enabled != null),
        controller = controller ?? SwitchController(),
        super(
          key: key,
          initialValue: initialValue ?? controller.getValue(),
          autovalidate: autovalidate,
          onSaved: onSaved,
          validator: validator,
          enabled: enabled,
          builder: (FormFieldState<bool> field) {
            return SwitchField(
              key: key,
              enabled: enabled,
              controller: controller,
              leadingLabel: leadingLabel,
              trailingLabel: trailingLabel,
              onChanged: onChanged,
              activeColor: activeColor,
              activeTrackColor: activeTrackColor,
              inactiveThumbColor: inactiveThumbColor,
              inactiveTrackColor: inactiveTrackColor,
              activeThumbImage: activeThumbImage,
              inactiveThumbImage: inactiveThumbImage,
              materialTapTargetSize: materialTapTargetSize,
              dragStartBehavior: dragStartBehavior,
              errorText: field.errorText,
            );
          },
        );
  final SwitchController controller;

  @override
  _SwitchFormFieldState createState() => _SwitchFormFieldState();
}

class _SwitchFormFieldState extends FormFieldState<bool> {
  @override
  SwitchFormField get widget => super.widget;

  @override
  void initState() {
    super.initState();
    if (widget.controller.getValue() != widget.initialValue) {
      widget.controller.setValue(widget.initialValue);
    }
    widget.controller.addListener(_handleControllerChanged);
  }

  @override
  void didUpdateWidget(SwitchFormField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.removeListener(_handleControllerChanged);
      widget.controller.addListener(_handleControllerChanged);
    }
    setValue(widget.controller.getValue());
  }

  @override
  void dispose() {
    widget.controller?.removeListener(_handleControllerChanged);
    super.dispose();
  }

  @override
  void reset() {
    super.reset();
    widget.controller.setValue(widget.initialValue);
  }

  void _handleControllerChanged() {
    if (value != widget.controller.getValue()) {
      didChange(widget.controller.getValue());
    }
  }
}
