import 'package:flutter/material.dart';

/// value data for tree item
class TreeValue {
  /// item id must be unique
  final Object id;
  String text;
  bool checked;
  bool expand;

  /// user data string
  String tag;

  /// user data flags
  int flags;

  /// user data dynamic
  dynamic data;
  TreeValue({
    this.id,
    this.text = "",
    this.checked = false,
    this.expand = false,
    this.tag = "",
    this.flags = 0,
    this.data,
  })  : assert(text != null),
        assert(checked != null),
        assert(expand != null),
        assert(tag != null),
        assert(flags != null);
}

/// value controller for TreeValue
class TreeValueController extends ValueNotifier<TreeValue> {
  TreeValueController({
    Object id,
    String text = "",
    bool checked = false,
    bool expand = false,
    String tag = "",
    int flags = 0,
    dynamic data,
  }) : super(TreeValue(
          id: id,
          text: text,
          checked: checked,
          expand: expand,
          tag: tag,
          flags: flags,
          data: data,
        ));
  List<TreeValueController> _children;

  void _push(TreeValueController child, TreeItemSort sort) {
    if (_children == null) {
      _children = List<TreeValueController>();
    }
    if (child._parent != null) {
      child._parent._remove(child.id);
    }
    child._parent = this;
    _children.add(child);
    if (sort != null) {
      _children.sort(sort);
    }
    notifyListeners();
  }

  void _removeAndNotifyListeners(Object id) {
    if (_remove(id)) {
      notifyListeners();
    }
  }

  bool _remove(Object id) {
    if (id == null) {
      debugPrint("TreeValueController remove id null");
      return false;
    }
    if (_children != null) {
      for (var i = 0; i < _children.length; i++) {
        if (_children[i].id == id) {
          _children.removeAt(i);
          return true;
        }
      }
    }
    debugPrint("TreeValueController remove id == $id not found");
    return false;
  }

  TreeValueController _parent;
  TreeValueController get parent => _parent;
  Object get id => value.id;
  String get text => value.text;
  bool get checked => value.checked;
  bool get expand => value.expand;
  String get tag => value.tag;
  int get flags => value.flags;
  dynamic get data => value.data;
  set text(v) {
    if (v != value.text) {
      value.text = v;
      notifyListeners();
    }
  }

  set checked(v) {
    if (v != value.checked) {
      value.checked = v;
      notifyListeners();
    }
  }

  set expand(v) {
    if (v != value.expand) {
      value.expand = v;
      notifyListeners();
    }
  }

  set tag(v) {
    if (v != value.tag) {
      value.tag = v;
      notifyListeners();
    }
  }

  set flags(v) {
    if (v != value.flags) {
      value.flags = v;
      notifyListeners();
    }
  }

  set data(v) {
    if (v != value.data) {
      value.data = v;
      notifyListeners();
    }
  }

  /// if has children return true
  bool get children => _children != null && _children.length != 0;

  /// clear all children
  void clear() {
    if (children) {
      _children.clear();
      notifyListeners();
    }
  }

  void forEach(ForEach f) {
    f(this);

    _forEach(f);
  }

  _forEach(ForEach f) {
    _children?.forEach((ele) {
      ele.forEach(f);
    });
  }

  void forChecked(ForEach f) {
    if (checked) {
      f(this);
    }
    _forChecked(f);
  }

  void _forChecked(ForEach f) {
    _children?.forEach((ele) {
      ele.forChecked(f);
    });
  }

  void forTopChecked(ForEach f) {
    if (checked) {
      f(this);
      return;
    }
    _forTopChecked(f);
  }

  void _forTopChecked(ForEach f) {
    _children?.forEach((ele) {
      ele.forTopChecked(f);
    });
  }

  Iterator<TreeValueController> get iterator {
    if (_children == null || _children.length == 0) {
      return _Iterator();
    }
    return _children.iterator;
  }
}

class _Iterator extends Iterator<TreeValueController> {
  @override
  bool moveNext() {
    return false;
  }

  @override
  TreeValueController get current => null;
}

/// forEach if true  exit forEach
typedef ForEach = void Function(TreeValueController element);

/// Item Builder
typedef TreeItemBuilder = Widget Function(
  BuildContext context,
  TreeValueController item,
);

/// Sort Item
typedef TreeItemSort = int Function(TreeValueController, TreeValueController);

/// Tree Controller for TreeView
class TreeController {
  final _keys = Map<Object, TreeValueController>();
  final TreeItemSort sort;
  final root = TreeValueController(
    text: "root",
    expand: true,
  );
  TreeController({
    this.sort,
  });

  /// if has item.id return true
  bool containsKey(Object id) => _keys.containsKey(id);

  /// return item controller
  TreeValueController controller(Object id) => id == null ? root : _keys[id];

  /// push a item to tree
  bool push(Object parentID, TreeValue value) {
    final id = value.id;
    if (id == null) {
      debugPrint("TreeController not support push id == null");
      return false;
    } else if (_keys.containsKey(id)) {
      debugPrint("TreeController id == $id,already exists");
      return false;
    }
    TreeValueController parent;
    if (parentID == null) {
      parent = root;
    } else {
      parent = _keys[parentID];
      if (parent == null) {
        debugPrint("TreeController parentID no found $parentID.");
        return false;
      }
    }
    final child = TreeValueController(
      id: id,
      text: value.text,
      checked: value.checked,
      expand: value.expand,
      tag: value.tag,
      flags: value.flags,
      data: value.data,
    );
    _keys[id] = child;
    parent._push(child, sort);
    return true;
  }

  /// remove item from tree
  bool remove(Object id) {
    if (id == null) {
      return false;
    }
    final controller = _keys[id];
    if (controller == null) {
      return false;
    }
    controller._parent._removeAndNotifyListeners(id);
    _keys.remove(id);
    return true;
  }

  /// clear all item
  void clear() {
    _keys.clear();
    root.clear();
  }

  /// if not have any children return true
  bool get empty => _keys.length == 0;

  forEach(ForEach f) {
    root._forEach(f);
  }

  void forChecked(ForEach f) {
    root._forChecked(f);
  }

  void forTopChecked(ForEach f) {
    root._forTopChecked(f);
  }
}

/// tree view
class TreeView extends StatefulWidget {
  TreeView({
    Key key,
    @required this.controller,
    @required this.builder,
    this.emptyBuilder,
    this.left = 20,
  })  : assert(controller != null),
        assert(builder != null),
        assert(left >= 0),
        super(key: key);
  final TreeItemBuilder builder;
  final WidgetBuilder emptyBuilder;
  final TreeController controller;
  // children left padding
  final double left;
  @override
  _TreeViewState createState() => _TreeViewState();
}

class _TreeViewState extends State<TreeView> {
  TreeValueController get root => widget.controller.root;
  @override
  void initState() {
    super.initState();
    root.addListener(_handleControllerChanged);
  }

  @override
  void didUpdateWidget(TreeView oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.root.removeListener(_handleControllerChanged);
      root.addListener(_handleControllerChanged);
    }
  }

  @override
  void dispose() {
    root.removeListener(_handleControllerChanged);
    super.dispose();
  }

  void _handleControllerChanged() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final children = root._children;
    if (children == null || children.length == 0) {
      return widget.emptyBuilder == null
          ? Text("")
          : widget.emptyBuilder(context);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children.map((item) {
        return _TreeItem(
          key: widget.key,
          builder: widget.builder,
          controller: item,
          left: widget.left,
        );
      }).toList(),
    );
  }
}

class _TreeItem extends StatefulWidget {
  _TreeItem({
    Key key,
    @required this.builder,
    @required this.controller,
    @required this.left,
  })  : assert(builder != null),
        assert(controller != null),
        assert(left >= 0),
        super(key: key);
  final TreeItemBuilder builder;
  final TreeValueController controller;
  // children left padding
  final double left;
  @override
  _TreeItemState createState() => _TreeItemState();
}

class _TreeItemState extends State<_TreeItem> {
  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_handleControllerChanged);
  }

  @override
  void didUpdateWidget(_TreeItem oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.removeListener(_handleControllerChanged);
      widget.controller.addListener(_handleControllerChanged);
    }
  }

  @override
  void dispose() {
    widget.controller.removeListener(_handleControllerChanged);
    super.dispose();
  }

  void _handleControllerChanged() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final children = widget.controller._children;
    if (children == null ||
        children.length == 0 ||
        !widget.controller.value.expand) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          widget.builder(context, widget.controller),
        ],
      );
    }
    final double left = widget.controller.value.id == null ? 0 : widget.left;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        widget.builder(context, widget.controller),
        Padding(
          padding: EdgeInsets.only(left: left),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: children.map((item) {
              return _TreeItem(
                key: widget.key,
                builder: widget.builder,
                controller: item,
                left: widget.left,
              );
            }).toList(),
          ),
        ),
      ],
    );
  }
}
