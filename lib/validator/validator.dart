import 'package:flutter/material.dart';

/// field validator interface
/// 定義了 驗證器 接口
abstract class ValidatorState {
  /// dirty
  /// dirty 狀態 記錄 field 是否被操縱過
  bool dirty;

  /// if invalid == true && initialValue not change, validator is invalid validator function should return null
  /// 如果 invalid == true 並且 initialValue 沒有改變，則驗證器 無效 此事 驗證函數 應該 return null
  ///
  /// * if _skipDirty == ture then invalid return false
  /// * if _skipDirty == false then invalid = !dirty
  bool get invalid => _skipDirty ? false : !dirty;
  bool _skipDirty = false;

  /// if true auto validate
  /// 是否 自動驗證
  bool autovalidate;

  /// retrun field initial value
  get initialValue;

  /// return to FormFieldValidator compatible functions
  FormFieldValidator get validator;

  /// return to FormFieldSetter compatible functions
  FormFieldSetter get onSaved;

  String errorText;
}

typedef FieldValidator<T> = String Function(
  FieldValidatorState<T> validator,
  T value,
);

/// FormField autovalidate support dirty
class FieldValidatorState<T> extends ValidatorState {
  /// if true auto check invalid
  ///
  /// if false you should check invalid and set dirty on your FieldValidator function
  final bool autoinvalid;

  /// retrun field initial value
  @override
  T get initialValue => _initialValue;
  final T _initialValue;

  /// return to FormFieldValidator compatible functions
  @override
  FormFieldValidator get validator =>
      _validator == null ? null : _fieldValidator;
  String _fieldValidator(T v) {
    if (autoinvalid) {
      // deal diry and invalid
      if (invalid) {
        // initialValue changed set dirty
        if (_initialValue != v) {
          dirty = true;
          errorText = _validator(this, v);
          return errorText;
        }
        return null;
      } else if (!dirty) {
        dirty = true;
      }
    }
    errorText = _validator(this, v);
    return errorText;
  }

  final FieldValidator<T> _validator;

  /// return to FormFieldSetter compatible functions
  @override
  FormFieldSetter get onSaved => _onSaved == null ? null : _fieldOnSaved;
  void _fieldOnSaved(T val) => _onSaved(val);
  final FormFieldSetter<T> _onSaved;

  /// Create FieldValidatorState
  FieldValidatorState({
    @required T initialValue,
    FieldValidator<T> validator,
    FormFieldSetter<T> onSaved,
    bool dirty,
    bool autovalidate,
    bool autoinvalid = true,
  })  : this._initialValue = initialValue,
        this._validator = validator,
        this._onSaved = onSaved,
        this.autoinvalid = autoinvalid,
        assert(initialValue != null) {
    if (dirty == null || !dirty) {
      this.dirty = false;
    } else {
      this.dirty = true;
    }

    if (autovalidate == null || !autovalidate) {
      this.autovalidate = false;
    } else {
      this.autovalidate = true;
    }
  }
}

/// Syntactic sugar Map<Field,Validator>
class FormValidatorState {
  Map<String, ValidatorState> fields;
  FormValidatorState({this.fields});

  /// reset all Field.dirty = false
  void reset() => fields?.forEach((_, filed) {
        filed.errorText = null;
        filed.dirty = false;
      });

  /// if Field exists return filed.autovalidate else or false
  bool autovalidate(String key) {
    if (fields == null) {
      return false;
    }
    var filed = fields[key];
    return filed == null ? false : filed.autovalidate;
  }

  /// if Field exists return filed.initialValue else or null
  initialValue(String key) {
    if (fields == null) {
      return false;
    }
    var filed = fields[key];
    return filed == null ? null : filed.initialValue;
  }

  /// if Field exists return filed.validator else or null
  FormFieldValidator validator(String key) {
    if (fields == null) {
      return null;
    }
    var filed = fields[key];
    return filed == null ? null : filed.validator;
  }

  /// if Field exists return filed.onSaved else or null
  FormFieldSetter onSaved(String key) {
    if (fields == null) {
      return null;
    }
    var filed = fields[key];
    return filed == null ? null : filed.onSaved;
  }

  /// if Field exists return filed.errorText else or null
  String errorText(String key) {
    if (fields == null) {
      return null;
    }
    var filed = fields[key];
    return filed == null ? null : filed.errorText;
  }

  /// call validator for all Field
  bool validate(FormState form) {
    if (fields == null) {
      return form.validate();
    }
    fields.forEach((_, filed) => filed._skipDirty = true);
    bool yes = form.validate();
    fields.forEach((_, filed) => filed._skipDirty = false);
    return yes;
  }

  /// save form
  void save(FormState form) {
    if (fields == null) {
      form.save();
      return;
    }
    fields.forEach((_, filed) => filed._skipDirty = true);
    form.save();
    fields.forEach((_, filed) => filed._skipDirty = false);
  }
}
