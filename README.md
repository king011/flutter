# flutter

General package for Flutter.validator for form validate.widget for some diy widget.

# validator

validator autovalidate carrying dirty state

validator 提供了 表單驗證的 封裝 主要解決 flutter 設置 autovalidate 是 沒有爲 Field 記錄 dirty 狀態 

![](images/validator.gif)

# widget

widget some widget

* SliderField/SliderFormField Slider on form
* SwitchField/SwitchFormField Switch on form
* TreeView tree view and controller

## TreeView
![TreeView](images/TreeView.gif)